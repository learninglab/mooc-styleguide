/*!
 * Plugin javascript Inria Learning Lab pour FUN-mooc.fr
 * https://learninglab.inria.fr/
 *
 * Copyright (c) 2019 Inria Learning Lab
 * Released under the MIT license
 */

(function ($) {
  var defaultLang = 'fr';
  var currentLang = getLang();
  var $body = $('body');

  // Apply lang as soon as possible but some content may not be ready
  applyLang();
  addCopyCode();

  $.getScript("https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js", function() {
    applyHighlight();
  });

  // Apply lang when window is fully loaded
  $(window).load(function(e) {
    applyLang();
    addCopyCode();

    // add custom icon for Vidéos, TPs, exercices, etc
    $('[data-page-title*="TP"], [data-page-title*="Exercice"], [data-page-title*="Exercise"], [data-page-title*="Practice"]').removeClass('seq_other').find('.fa').addClass('fa-edit');
    $('[data-page-title*="Cours"], [data-page-title*="Lecture"]').removeClass('seq_other').find('.fa').addClass('fa-file-text-o');
    $('[data-page-title*="Video"]').removeClass('seq_other').find('.fa').addClass('fa-video-camera');
    $('[data-page-title*="Forum"]').removeClass('seq_other').find('.fa').addClass('fa-comments-o');
    $('[data-page-title*="Introduction"]').removeClass('seq_other').find('.fa').addClass('fa-bookmark');

    // add custom icon base en data-unit-icon attribute
    $('.seq_contents').each(function() {
      var $seq = $(this);
      var $content = $("<div/>").html(DOMPurify.sanitize($seq.text()));
      var $iconAttr = $content.find('[data-unit-icon]');

      if (!$iconAttr.length) {return;}

      var icon = $content.find('[data-unit-icon]').data('unit-icon');
      var $navItem = $('#tab_'+$seq.attr('id').split('_').pop());
      var $icon = $navItem.removeClass('seq_other').removeClass('seq_problem').css({'line-height': '42px'}).find('.fa');

      if (icon.indexOf('ill-icon') !== -1) {
        $icon.removeClass('fa').addClass('ill-icon').addClass(icon).css({'line-height': 'initial'});
      } else {
        $icon.addClass(icon).css({'line-height': 'initial'});
      }
    });

    // Add fallback link to all LTI iframes
    $('.ltiLaunchFrame').each(function(){
      var url = $(this).attr('src');
      $(this).after('<p></p><p>If the iframe is blocked or is not working, please try this <a href="' + url + '" target="_blank">link</a></p>');
    });
  });

  // Apply lang when problem is append
  $body.on('DOMNodeInserted', '.problem', function(e) {
    applyLang();
    addCopyCode();
    if ($(e.target).hasClass('show-label') || $(e.target).hasClass('problem')) {
      applyHighlight();
    }
  });

  $('.set-lang').click(function() {
    setLang($(this).data('lang'));
    applyLang();
    addCopyCode();
  });

  function openFullscreen($target){
    $body.addClass('ill-fullscreen');
    $target.addClass('ill-fullscreen').find('.ill-icon-maximize-2').removeClass('ill-icon-maximize-2').addClass('ill-icon-minimize-2');
    $target.find('.floating-btn').contents().filter(function() {
      return this.nodeType === 3
    }).each(function(){
      this.textContent = this.textContent.replace('Open','Close');
    });
    $target.prepend('<button class="btn btn-primary toggle-fullscreen close"><i class="ill-icon ill-icon-minimize-2"></i></button>');
    var $iframe = $target.nextAll('iframe').length > 0 ? $target.nextAll('iframe') : $target.closest('.xblock').find('iframe').length ? $target.closest('.xblock').find('iframe') : $target.closest('.course-content').find('iframe');
    $iframe.first().addClass('ill-fullscreen');
    $iframe.first().focus();
  }

  function closeFullscreen($target){
    $body.removeClass('ill-fullscreen');
    $target.removeClass('ill-fullscreen').find('.ill-icon-minimize-2').addClass('ill-icon-maximize-2').removeClass('ill-icon-minimize-2');
    $target.find('.floating-btn').contents().filter(function() {
      return this.nodeType === 3
    }).each(function(){
      this.textContent = this.textContent.replace('Close','Open');
    });
    $target.find('.close').remove();
    $('iframe').removeClass('ill-fullscreen');
  }

  $(document).off('click','.toggle-fullscreen').on('click','.toggle-fullscreen', function() {
    var $target = $(this).closest('.title-banner');
    if ($body.hasClass('ill-fullscreen')) {
      closeFullscreen($target)
    } else {
      openFullscreen($target)
    }
  });

  // Display "Powered by OVH" below Jupyter Notebooks in Scikit-Learn Mooc
  if (window.location.href.indexOf('41026') !== -1) {
    $('.toggle-fullscreen').each(function(){
      var $target = $(this).closest('.title-banner');
      var $iframe = $target.nextAll('iframe').length > 0 ? $target.nextAll('iframe') : $target.closest('.xblock').find('iframe').length ? $target.closest('.xblock').find('iframe') : $target.closest('.course-content').find('iframe');
      if ($iframe.first().attr('title').indexOf('Notebook') !== -1) {
        var img = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjQAAABZCAMAAAAeoE6cAAAAwFBMVEX///8ADpwAAJoAAJcAB5s1Pq4AAJsACpvr7PfHy+qdodczO62ztNp6fsaDic3x8vqnqthsbrhydLpeYrarrdh/gcBCRqvNz+s8Q66HjMz7+/7z9Pvt7vnm5/Xa3PG1ueLR0+zg4vOPlNBRWbggKKRGSq28wOUUIKRnarksNKkLF6B8gcZQVrWKjsrW1+2VmM8fJqIOH6ZfZbqhotDAwN5YW7MtMqVqbLdQU663uNsnLKNrbbc5PqhaYb01P7EsObATuYhMAAAWl0lEQVR4nO1daVvivteWJLYCIqgMSNlaZRE3XNCZ4Rn/8/2/1VNy0jZNs5yC89OZi/uVAk3T5s7J2XJycLArorv6IRKT9s532+OfwAllSNCLo8/u7B5fAg/Er+Dge+PP7uweXwN3BMmZCrn77L7u8TUw9tCChjx8dmf3+GjUoi0uOvqFFzQ9e1PBfvH663A0udziqkuK5sx6ZG9qNR9u1fE9Pg/31OuWvmhIsYtThbbsTQ069NuWXd/jkzB8ZOSq9FUN/OK0DuxNvVA22WaB3OPz0IjXGVpWq1gxvLm9sjfVvGEV2ti6+/8g+n3HNPsc9Pvpn82NEUSmg3INXOAFzXfHG+jFTfm05P3/YQTH53e9r6fkDXtXzz+ToVzy4ScvpVpoo7Vg33NowQ+PLP4Z3UYX/zfR9gihF333D/9TNK8pId4M/hlOCYxtGW4P6x6WNNRhbh+8c/6R6XZxhmA4CqvVVYxqNRy5nqFZrvH8KjFEv6FoF7HZvNoMSGl94U+jujF8yBr+WYlYQCmtYllCC3a86i4VAsmh+ejQH99fT+tz5hFKicce69Pv9zPzFB2dvp5W8a1H317Ps6Fr9qbTBkrViE4vftS2V0qODglmrv3XOIZxgn8S5cSvzNANPOB9wd6xvan+VNyf3JV7z8Ho8jeNuUII83lvfJ/F/8QfXbyMtE11K5tfo5+y6cU/p7Xk3/Xmvx+IRSNabH75jL1NAUd1TpqTrRv4M2hLpBmlygl5Ri+j73hB03FQoZ3yj5ayumenMQUM96TzW51Aeaa8Q9g7XMLPhaCsUj4FEIKqxX9Jt9ZJ/gLSnGQaLcWmvJQIOtHQ3tTRJB35MgJ5dEVsEXZGvNMiBW9YGW6CCPSZUONrvJ/0p/vCW/5GaXl3qcDXJ026Omxe9RynJzbx0W2notTLOIuXAf2aJ1HGj9eleGFi8SolfUhv2qqMg4UYOxoz3hpbC44J0jgW2w0aQJoSylMeX5801SdpwpIl6vIXNGfY2hHdHs1ZNs5z5OQcTElylR8rKZWbm/nk8HAyv4kVYpL2LRY2ygrRFmYayrQJbsETkei+e9JkpFnK/hY2DxFXN9GpV5VMjdQjOJX5h+Ts7DHpgE/J4bfWKCFBPxxfnh2S5Il8Osl7iKIFX3A8xBITD11lQ0yfJebTnjQpafqdnNSg14ire2i/HvMcWnCYU47IK2Z5HM9FlxmljVBVUIKoekoTUpF6mPtSER722/CnZGnYbE+alDRDhQCIR63efFjQ6WBC8r9H6KhtLzHRn3qGdSZqVJLf0JysATXF9zBuREgWypTzPWlS0rQU0pA752Q/x5vb1w6zc6xS1u1DGa3h9j69sozK6pdomXRyBIGLXYvmBiFMDZLSeE+alDQFhwtxpL4czPBBpxuHYjvsKHcnTo9YUIdLmG9/p82GWKLItbwWQd4YuXDdJv4lrGSv6dV70iSkCSYsP2yxLmyX3c063ty+dXSjEItgc1fPe4IzFefg9YS6lEsAG/IP/RvngAo+k+w2e9IkpNHoJ46hruEXJ1cK56DgInQqNVVu0sSGtnsdEyZzhaylNvvPwAVnRL0Lv6tnKtGeNAlpasVYtV+xLSqjG1U0meB7Lv/ydXGhcy2OcxGQR6VxPEP79ExaoI45UUndFa8G9yU5yz7ZkyYhjS5YndcCFGgG2gDScajU3XmRfw4RIPQp8owLNi+AYkwSNcNffJoQx4gGkP8shwL2pBGkCb7rFhvLi6mWSCZ3vLS+zgoj1rVRhDx8gsyKmUFGai7g3ECpwuDypBPpoz1pBGmOpjrSkEfTddqBNnDGZQiNtbc+t/FBpCXTe+xzPovVTPLnPPBHdySX9l+LpvmeNII0kWaF2PTX5M4/RgcQ2MQRdArWWtLULZpwAPsfyCE+gQ4EI5EHH6w/4yNygH3AHuVn2JNGkOZBLzhMe9v6aC3YbZ/oU//8Smi+ZACCBhmh4gAVjJxLTkZwZ5JXm9/xHn5zKqtOe9II0vw0qLVEn9Bwjw86VZQxGXXzGBtyjG1ZKCIaNC+Rq7USfpkw++iBu6Z833Kj4IlPDpIz//akEaQxbqzVjl34iBc0SlZ0laowKEfUYkyD97pU/YkBmM657LKGM6tGsPMp9+GeNII0tya9VnKfpwjO8H49VfijN0lZlNzgDYatVJa+iGu/Sx8BJXxLAB7UfSXi/5Gk6VdblzFq46pOamJI8zDjLSzHf6oaR1Qdt5aXrXE3fU0Jaa6NY6nZBbXaPuikhkUtpDFn+s0q+BB1iqpgiPyZyPo0+hFBoqre6Y8izVH1+WmT3w6gk9pI1a4cpAmG7Qsva4Gse6He5OzeVe5Ccze7V5Ur7dfBqH2V3oCSqxUYHglprsykWagdCfBbKump8ha0lr3+vuaEnhdwy92VStgOhFdYNrhuhSpsvBFIp6v8G/gY0gyXF5RIG5p9Qr1r5Zd20nQbFUqyvFY/boG8FjJbY0QeYdQ3blQceZRRXdhmdurlbhAT+3jTfEKaC7OSUghB4YNOjCnCYIkWNBVLIQLhcCthO23wu7imjTwwqEP9FSIDWpW1H0GaoLUmhdfok8plbiLYSDN4viGFQfMJOSzeje8YMCfr86lT1AaOGl7Br0K8q2FGmoXZ7ZJm4QsM3/DSQklXGaL3Lthctf1rNe6MwmlR7U1aMtAv5N+ytTIJP4A0g2uqtRl9eijfzEKa2ULvkveJd6IKG+7tN6c0cQ2VqBVe9DfgibMJaea24TvL9aKBDzqpidv4S+OLfxue8WDIl0f/Br+jj+MF1qLcqicWuqleFTiFK1SP9u6kOXpOXgTLNBL4gE4l1phJM/PF7/2sAZZ8cqbQg08Xs4tdS5pxpXgDGJdFlOywtA2fT+T86xF+4NX0uxU6PbRSsHMlgPeavTmqCah40CgwwzdQhbWumj7IxcKY70yaZke8Q0Yn5/fLl/G4dnnbEYNCDzMzyEialmCIT72722VrPB4v78/XCW2ostdxC9LM0lRab/H9vh338P72ArwjpCJSmaykSfd6b6CPbOovU3YvBc/oSyuqnSMDNBGvU7JMwEhIldwLFVk1WqW7BavTXJX2O5PmVuxZpzfHmcE0XHUoMDhLCjCRpisSQwg96abCvD9qeWJFIXkFpTxpBo9CrNDnMN3YHP085IuqL7hpJ42scx6j1RI2V0SBmgdsh5k0QmT8MH1vAAgoL6c0HIRgiFc0L1RMj6K+XXqHpUKaFrxCwtTSALW1ckcDaYKEM89K7C1Y3ojAbE7GlyZN8F2YmlNFBI9lffbgxjqAXho9DPA5nmodz/6ijKCJB9LwjMLjQs5N3xsw5F1n61D+MIBO6VK+HngIQbNvD0hDGi0nhBc6T5om7G4gb0XfZJezJjNkDKS5F/H5ZVG1nTGNl6A0aVZiYe4UAsIjabU4eHIQIGkSbzOzR0ULxtckgeuNOs1Mr5+6AD4i9pgnQaIeF9//UnxTcHwIl4PnBshxhTQQsWFaPX5UIbHimbJJTxoo/WQorjuDdIVcuKQ0acBrRzoa302Q6vCVg0fHEJKQXxKt0UEn1c86mpQjDV0Xewz4qTGDEIDokz/PD2EIUctK0Qn/ZHIX4/1U4klyd+xDcrNh80x4xx6ztAA9aU6ERq8nAcwCJsvpsqSJhPNca2n00+37Nj9NBbrIr+jhF6epMnfx6aGiAaOf5ng70jR5IKlQe0eElwoJHKLCEi16WHcjTZt7zIipJlozGmZ31JNGrB2mWNOksCaWJQ2w0uQNfEjynw5c7n3f28y4Lv7YDHVvyENJzlg8wluSRnjy1P3bSdRS/Tn8Wo2DbLAbafgQ+T6q2JeWNBC+M2+b7kLGvPR9SdI0O6D8mTLcEoPmwGkN8+xwfORIzfEsZ25XKjZSbLk8CdIwRdIEwj2ikDzieee+Lu9ckEbyy5ngF5uOuBqMrFehJQ2ngE+MWUCwDBNJiS1Jmurcr1SKLuIMwqJBbOWPpVULPfCFCHQbX18iuZ8xtLTaThE2LE9J1FLhYBt0wVfN4AJp2FvHiceidxDkALJqk440TT5ztR0T4FtCfZbdtSRpoH6XpYReEkZw13X1vWoJQaOoCEE5c5s3YfSDdLczuQevEH1QF4ZQbIDKqX3BuzkqJfw0L4MjF06LJjfoqa6qYAI60lS5eWTLdK0CLzMNvhxpYG8hW5tzCNIcYbfK4c/RwoKoBj7+xI0U5jMadPEABMDkVq2neOrChoN8SueQS0Zf9U9y7OQRBjJSZJc1pPm5oURWLEeDgEdrpDIq5UgD5pEtLTIhzQhRDBi/wKg5dVFho7j7ZnI2bx5JPKBkqdUInHsTk7Mu155Ys7RvbifSrDcfmYOxeehIw0WV/2jL0+OFZMnhlqQZcFFmSxdMt7Dgs37dIIfKXfCWegq2MCcNcfp6FyXLv0MYgRwWPFYRxFFzZRsh212/RO5EGlgL342X5KAhTfCNU//NNmOuYR3ekjRH8OiWbdEJaYSM/hD4TJkGeEs9g0XTixYQ5S6ZFAtxzoL/6CBdMqT8UihaStba97wTaUrlj+lIwyNi3i/bZSJJLX3QcqSB6lZGN9CBtJf728eRhtwq0wCvQMuNmF9lR6/ROrAyqkLwjVdP37KoM2HIbd+FNGJEELWUNtCQBjYk2zditHYiDaiM1JJDkFWN+DDSFDLd2ts0rSb9ye8NjJKymXs9se+t+E2TKwFScQtYyYo6M2AX0oTOaSzDTBrroVzg/cz2G5cjjTBOLbuiU9KM/fJLiB7qNBpsI2isdWcg9Fk2R3hqiBekDWb2hth7aai2/+VJEym3LUcaSCKgGNIYNnOXRyGqspUM85nFZdraZjdCErLRkRFqnbCnRETaC/J9KmmaYH5hSJM+6R8jzQH+wFv7aPvKqByVSCbPQOqWlwK78nMlINwQaar6gwpEbQix7UA4rYiBlP+4pAnxOk2ppG/baKtlhm63atdaui2ABI1y211rkDIwsXxJxJfgITDGKb48aYbKsJcjDcwuanFoZKQJP4Q0PlG0YHyt4RyoNWtc+PiR3g6OpNCMvoZFIOeQR7DbweiS3oU00X9hPVV3sp6OnIGOjDTNp49QalQJUT66zcEmVs8dKKqF3EAbQh4E0UatNxC7VXgccSUcOqYufK6fhnvuPNV9moPQ47clDTyexRDJSFNiV78ZRHXT4msN59s5s8YIml6xRJEDwuAuZr0CIMhHfm3kpJBjxgIEX94jzG/Lnrb1CPO1wXaQKCzmN5s/t3KnqIOtOJ/7+Dx0azsKxOkb5A4tahKamVISBj+gxZ/JKSG+eYH8gNiTTc2XYI49Wc87gYjjxZakgfrQOn9Wgh50YfPn6G3n9amQormk+awkZDvMUIArxU/BAbR/TyTwi1RnDS5TNUkklJsXgK8e5T6AIFsW5f4/K2mCcyXKzRdA25FbENtaZ3/vgoLuOLz6dSGjY98pk8KZ9jCAMuLMQ7pqHsALZXkTQ1H69SjouDixE2lqu+bTdHnGgK0o3QNR8mlAlJgOtgEhKyVh8Vlj0yovJF283GY2DaiqiQTDHI4Mlf2KDTlPQ6wJrwuuvGcg5oOtJNuhcOh1oTSjpcj6TqQRGVK4ftsy98zOtxoYf1lojieKkwtDsTkwFiUWHrtkGYV8HfhnKzdcBueigpVlPnOnykDOve889YNDlAgk3y3tgsrudUQmjWVUdyJNBMkuOG1MmyMsMt6N/Ie0Vlnl56LD9w3OIZglklHxwLMIzLYIvCkqSLmjf885fbpInw3mXHBRk5ZgTsCbwWxg5ryuGEM5I9V6yPRue7lhNwJDOSa1pIEFwWyAid0Ip+oV1JAVcAzanCRY+HpllrXwBMmuqHA7R5wAWTvkQ99cbisH50E/G4hSwhXy6MyQaCU7He3Z3PdS76wnr+5GGpGx7jpvmkMc5q7ue7JUujhID3WVbnpkSUMU2yl9IlFE6F2GqQuBunR9DHZRhZ1ntScHxztBzjEvdCgqtJC5/UyN/jIppdCxa82y79rmpdiRNMkOS5Pa1g2zv0F/UUdPHFV1pddqhOOTyZ8Jf3eo+32Yi6DAbYVtqv09CBops322A2msp29s0MdW0EJW7VwJAcK8hsV9HN0lZ4Qt9O8gg1RKkNjKmOxYaqQngmDa7kTP83mm4/a5ga4GwaD+seEAzupcs5tYHAPxQ0OzpsgYyREz8YTqVOcaUQ2F8jtNEvjOINwlsm1242goQVso7j5lbYNeObpM6sYx8+6y9HWkktA+AXYkjThcjbwV1Zrg2KOMZaEYcP96v5TeiDdJG0XRufKF2zPHD3GqLDkrsOYIFhdl33b0BnnCr4W5E4iXLi+Z7u1PJjhP142wBQBsVafzSBaeWNhMa5pZFN2/paflOs+ckpM47CvtrkWNXoQOP1El6sM1rxdE7obZTzcjypT89oAJ0/FdFQU1UZ9GSWETJ6LFiohi34YiQY6e5nkpzE16p8y0AHpYIb4k3Jt3W4oasnDVpXrHajQlKly9sLTOHKWTZRgNB/346YP+YBh1GzeUJj5u4j52cKNb06QL1nDpR1XCYrReTW/Uj6rv4kBo6SQGcZ6HV4sieVJ055546tNQqoTV9sQDF1x/VTEfCL1ajQa8+WAwmk1FufjiKeziTFFC39Ozzg+aDycEpqlC4/aWvhpn7BCdeaEpdm1EMCPZRgdCvcPp6/XZt29n16/TNUkZE69fC5SF+zM57BJzDuMuNfcuEnaSzu1yPJuNa8vrehJkIVIkPhDnWtHF4as8Tu2s5t7rt1obWnhKnpgWq+2cJEKZ0Pnr6X2M09d5csKAX3SWpKUhCHu9fBnPxq2X+9cnmugDSpQeaRarI+2yIAPs+VDEujmjgOjVyxqWClHKJXYJK67lWiSlse3JPB9Q3XP4mkyhuKeb6kdSXI7U5Z8nXvpNtYHc5ywZ8E0LhLeQSF0d54NGOruY+or8wvaRGOGbfAPoYqoMqIJsmz1KZsdRCnSpPsSB3Pm30f5FbV2ORfgzusmTovmpwa419w42+qckB30pp5/QRl5PyQpP5bP1ZuuszK/cgk88rY+136CGeDShJzpfRNjJ3UB6oaSoHuKrd0r3dZrbWC2YXJXLFo/RbC0YMfAmnr/TGX77rsirc4QmhEsWse1K7FrTWK792lwT8yek3lJ6GwulZKHIp3gePTNdxXJvYbASg9qjLsuAkIlBHzi69bU3uNAs9VF5s9t5eBz6fChWLIuIwexbxyvIm41y3OmVa+89nl30l2O/b3+z3juUZUC4ObGD6kt6DXv1fJcZJXc6zbC35rRhBfdV92yutuBdWYzEYW9B8zRghNYvzXbHqDfPH6wU68HXY+0crJU2u51ntaNL9TlbMr6Qh9q7x0+PghNDYngXt9WS270PhqceuQhdvwoXsV2GOqCsFvdD78Xb3GwFXRaY3IZ6b9OwNtk8TjFFNIja/yNSC0+XD/Y46HD1zOSjtm6eHW9o9PJb/v3/LiPTQlDW7HZ64wJsJJQsHC3Z0a/WereNs7PGba8dlqwpUQ4hVoBF46q1I4NxrRdj2Z7ZdfWBUTePZq1NCy3seU/9EG5ZG4c4TWA4Xvb4BdZ1/mFejjXO9IQQqQUrNZP3+JvQKmVB5Q4T1eIQ6wtGn5i8x9dDmcQa466QFNiMQCkVeo+/D2X27DvPaseeJufN/9QpjHv8JxhNCMOBWJJpAUuKa0it1brH34bZ+hCH365QUfC7jmpogauSuseXwf8DBx6jl18iMGwAAAAASUVORK5CYII=';
        var $poweredBy = $('<div class="poweredBy" style="color: #000e9c;text-align: right;"><span style="font-size: 10px;display: block;">Powered by</span><img style="height: 20px;width:auto;" src="'+img+'" /></div>');
        $poweredBy.insertAfter($iframe.first());
      }
    })
  }

  $(document).keyup(function(e) {
    if (e.key === "Escape") { // escape key maps to keycode `27`
      closeFullscreen($('.title-banner.ill-fullscreen'))
    }
  })

  $(document).off('click', '.course-content a[href^=#], .static_tab_wrapper a[href^=#]').on('click', '.course-content a[href^=#], .static_tab_wrapper a[href^=#]', function(e) {
    e.preventDefault();
    var aid = $(this).attr("href").replace('#','')
    $('html,body').animate({scrollTop: $('[id="'+aid+'"]').offset().top - $('#top-menu').height()},'slow');
  });

  $('.external-resource').each(function(){
    var $externalResource = $(this);
    getExternalResource($externalResource.data('url'), function(content){
      if ($externalResource.data('inbrief-icon')) {
        var icon = $externalResource.data('inbrief-icon');
        if (icon.indexOf('ill-icon') !== -1) {
          icon = 'ill-icon ' + icon;
        } else {
          icon = 'fa ' + icon;
        }
        var inBrief = `<div class="in-brief">
                        <div class="brief">
                          <div class="brief-img">
                            <i class="${icon}"></i>
                          </div>
                          <div class="brief-text">${content}</div>
                        </div>
                      </div>`
        $externalResource.html(inBrief);
      } else {
        $externalResource.html(content);
      }
      if ($externalResource.data('unit-icon')) {
        var icon = $externalResource.data('unit-icon');
        if (icon.indexOf('ill-icon') !== -1) {
          icon = 'ill-icon ' + icon;
        } else {
          icon = 'fa ' + icon;
        }
        var prefix = $externalResource.data('title-prefix') ? $externalResource.data('title-prefix') : '';
        var title = $externalResource.find('h1').hide();
        var titleBanner = `<h4 class="title-banner">
            <i class="${icon}"></i>
            <span>${prefix}${title.text().replace('¶','')}</span>
        </h4>`
        $externalResource.prepend(titleBanner);
      }
      if ($externalResource.data('hide')) {
        $externalResource.find($externalResource.data('hide')).remove();
      }
      if(typeof MathJax !== "undefined"){
        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
      }
      // Replace all links containing jump_to_id by its correct location
      const baseURL = window.location.origin+'/courses/'+$$course_id+'/jump_to_id';
      $externalResource.find('a[href*="jump_to_id"]').each(function(){
        console.log('')
        const link = this;
        link.href = baseURL + link.href.split('jump_to_id')[1];
      })
      addCopyCode();
    })
  });

  $('[data-block-type="vertical"]').each(function(){
    var id = $(this).data('usage-id').split('@vertical+block@')[1];
    var url = window.location.href.split('courseware')[0]+'jump_to_id/'+id;
    $('.studio-view').find('.jump-to-id').remove();
    $('.studio-view').find('.jump-to-id-btn').remove();
    $('.studio-view').append('<input class="jump-to-id" value="'+ url +'" readonly />');
    $('.studio-view').append('<button class="jump-to-id-btn" onClick="$(\'.jump-to-id\').select();document.execCommand(\'copy\')">Copy Link</button>');
  });

  function applyLang() {
    $wrapper = $('div.course-wrapper section.course-content div#seq_content .xblock-student_view-html,' +
      'div.course-wrapper section.course-content div#seq_content .xblock-student_view-problem,' +
      'div#content .xblock-student_view-static_tab,' +
      '.studio-xblock-wrapper .xblock-student_view');
    $wrapper.find(`[class*="text-"]:not(.text-${currentLang})`).hide();
    $('.text-'+currentLang).show().css("display","");
    $('.set-lang[data-lang="'+currentLang+'"]').prop("checked", true);
  }

  function copyCode(block) {
    navigator.clipboard.writeText(block.innerText).then(
        () => {console.log('copy')},
        () => {alert('Write to clipboard failed!')}
    );
  }

  function addCopyCode() {
    document.querySelectorAll('pre code').forEach((block) => {
      if (block.querySelectorAll('.copyCode').length > 0) return;
      const codeCopyIcon = document.createElement('i');
      codeCopyIcon.className = 'ill-icon ill-icon-copy copyCode';
      codeCopyIcon.addEventListener('click', () => {
        copyCode(block);

        // Rely on FUN tooltips
        const t = new TooltipManager();
        codeCopyIcon.setAttribute('data-tooltip', 'Copied!');
        codeCopyIcon.setAttribute('data-tooltip-show-on-click', 'true');
        t.openTooltip($(codeCopyIcon));

        setTimeout(() => {
          codeCopyIcon.removeAttribute('data-tooltip');
          codeCopyIcon.removeAttribute('data-tooltip-show-on-click');
          t.hideTooltip();
        }, 1000);
      });
      block.insertBefore(codeCopyIcon, null);
    });
  }

  function applyHighlight() {
    document.querySelectorAll('.problem pre:not(.hljs)').forEach((block) => {
      hljs.configure({
        languages: ['python']
      });
      hljs.highlightBlock(block);
    });
  }

  function getLang() {
    return storageTest() && localStorage.getItem('ill-lang') ? localStorage.getItem('ill-lang'):defaultLang;
  }

  function setLang(lang) {
    currentLang = lang;
    localStorage.setItem('ill-lang', lang);
  }

  function storageTest(){
    var test = 'test';
    try {
      localStorage.setItem(test, test);
      localStorage.removeItem(test);
      return true;
    } catch(e) {
      return false;
    }
  }

  function getExternalResource(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onload = function() {
      if (xhr.status === 200) {
        callback(DOMPurify.sanitize(xhr.responseText, {ADD_TAGS: ['iframe'], ADD_ATTR: ['target']}));
      } else {
        callback('<h5>Une erreur s\'est produite lors du chargement, veuillez réessayer.</h5>');
      }
    };
    xhr.onerror = function() {
      callback('<div class="ill-alert" style="margin:20px 0;">' +
        '<i class="ill-icon ill-icon-alert-triangle"></i>' +
        '<p class="text-fr">Une erreur s\'est produite lors du chargement, veuillez réessayer.</p>' +
        '<p class="text-en">An error has occured, please try again.</p>' +
        '</div>');
      applyLang();
      addCopyCode();
    };
    xhr.send();
  }
})(jQuery);

!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():e.DOMPurify=t()}(this,function(){"use strict";function e(e,t){y&&y(e,null);for(var n=t.length;n--;){var r=t[n];if("string"==typeof r){var o=r.toLowerCase();o!==r&&(Object.isFrozen(t)||(t[n]=o),r=o)}e[r]=!0}return e}function t(e){var t={},n=void 0;for(n in e)g(h,e,[n])&&(t[n]=e[n]);return t}function n(e){if(Array.isArray(e)){for(var t=0,n=Array(e.length);t<e.length;t++)n[t]=e[t];return n}return Array.from(e)}function r(){var o=arguments.length>0&&void 0!==arguments[0]?arguments[0]:_(),u=function(e){return r(e)};if(u.version="1.0.11",u.removed=[],!o||!o.document||9!==o.document.nodeType)return u.isSupported=!1,u;var h=o.document,y=!1,g=!1,v=o.document,D=o.DocumentFragment,R=o.HTMLTemplateElement,C=o.Node,H=o.NodeFilter,F=o.NamedNodeMap,z=void 0===F?o.NamedNodeMap||o.MozNamedAttrMap:F,I=o.Text,j=o.Comment,P=o.DOMParser,U=o.TrustedTypes;if("function"==typeof R){var W=v.createElement("template");W.content&&W.content.ownerDocument&&(v=W.content.ownerDocument)}var B=N(U,h),G=B?B.createHTML(""):"",q=v,V=q.implementation,Y=q.createNodeIterator,K=q.getElementsByTagName,X=q.createDocumentFragment,$=h.importNode,J={};u.isSupported=V&&void 0!==V.createHTMLDocument&&9!==v.documentMode;var Q=b,Z=T,ee=A,te=x,ne=S,re=M,oe=L,ie=null,ae=e({},[].concat(n(i),n(a),n(l),n(c),n(s))),le=null,ce=e({},[].concat(n(d),n(f),n(p),n(m))),se=null,ue=null,de=!0,fe=!0,pe=!1,me=!1,he=!1,ye=!1,ge=!1,ve=!1,be=!1,Te=!1,Ae=!1,xe=!0,Le=!0,Se=!1,Me={},ke=e({},["audio","head","math","script","style","template","svg","video"]),we=e({},["audio","video","img","source","image"]),Ee=null,Oe=e({},["alt","class","for","id","label","name","pattern","placeholder","summary","title","value","style","xmlns"]),_e=null,Ne=v.createElement("form"),De=function(r){_e&&_e===r||(r&&"object"===(void 0===r?"undefined":k(r))||(r={}),ie="ALLOWED_TAGS"in r?e({},r.ALLOWED_TAGS):ae,le="ALLOWED_ATTR"in r?e({},r.ALLOWED_ATTR):ce,Ee="ADD_URI_SAFE_ATTR"in r?e({},r.ADD_URI_SAFE_ATTR):Oe,se="FORBID_TAGS"in r?e({},r.FORBID_TAGS):{},ue="FORBID_ATTR"in r?e({},r.FORBID_ATTR):{},Me="USE_PROFILES"in r&&r.USE_PROFILES,de=!1!==r.ALLOW_ARIA_ATTR,fe=!1!==r.ALLOW_DATA_ATTR,pe=r.ALLOW_UNKNOWN_PROTOCOLS||!1,me=r.SAFE_FOR_JQUERY||!1,he=r.SAFE_FOR_TEMPLATES||!1,ye=r.WHOLE_DOCUMENT||!1,be=r.RETURN_DOM||!1,Te=r.RETURN_DOM_FRAGMENT||!1,Ae=r.RETURN_DOM_IMPORT||!1,ve=r.FORCE_BODY||!1,xe=!1!==r.SANITIZE_DOM,Le=!1!==r.KEEP_CONTENT,Se=r.IN_PLACE||!1,oe=r.ALLOWED_URI_REGEXP||oe,he&&(fe=!1),Te&&(be=!0),Me&&(ie=e({},[].concat(n(s))),le=[],!0===Me.html&&(e(ie,i),e(le,d)),!0===Me.svg&&(e(ie,a),e(le,f),e(le,m)),!0===Me.svgFilters&&(e(ie,l),e(le,f),e(le,m)),!0===Me.mathMl&&(e(ie,c),e(le,p),e(le,m))),r.ADD_TAGS&&(ie===ae&&(ie=t(ie)),e(ie,r.ADD_TAGS)),r.ADD_ATTR&&(le===ce&&(le=t(le)),e(le,r.ADD_ATTR)),r.ADD_URI_SAFE_ATTR&&e(Ee,r.ADD_URI_SAFE_ATTR),Le&&(ie["#text"]=!0),ye&&e(ie,["html","head","body"]),ie.table&&e(ie,["tbody"]),O&&O(r),_e=r)},Re=function(e){u.removed.push({element:e});try{e.parentNode.removeChild(e)}catch(t){e.outerHTML=G}},Ce=function(e,t){try{u.removed.push({attribute:t.getAttributeNode(e),from:t})}catch(e){u.removed.push({attribute:null,from:t})}t.removeAttribute(e)},He=function(t){var n=void 0,r=void 0;if(ve)t="<remove></remove>"+t;else{var o=t.match(/^[\s]+/);(r=o&&o[0])&&(t=t.slice(r.length))}if(y)try{n=(new P).parseFromString(t,"text/html")}catch(e){}if(g&&e(se,["title"]),!n||!n.documentElement){var i=(n=V.createHTMLDocument("")).body;i.parentNode.removeChild(i.parentNode.firstElementChild),i.outerHTML=B?B.createHTML(t):t}return r&&n.body.insertBefore(v.createTextNode(r),n.body.childNodes[0]||null),K.call(n,ye?"html":"body")[0]};u.isSupported&&(function(){try{He('<svg><p><textarea><img src="</textarea><img src=x onerror=1//">').querySelector("svg img")&&(y=!0)}catch(e){}}(),function(){try{He("<x/><title>&lt;/title&gt;&lt;img&gt;").querySelector("title").innerHTML.match(/<\/title/)&&(g=!0)}catch(e){}}());var Fe=function(e){return Y.call(e.ownerDocument||e,e,H.SHOW_ELEMENT|H.SHOW_COMMENT|H.SHOW_TEXT,function(){return H.FILTER_ACCEPT},!1)},ze=function(e){return!(e instanceof I||e instanceof j)&&!("string"==typeof e.nodeName&&"string"==typeof e.textContent&&"function"==typeof e.removeChild&&e.attributes instanceof z&&"function"==typeof e.removeAttribute&&"function"==typeof e.setAttribute)},Ie=function(e){return"object"===(void 0===C?"undefined":k(C))?e instanceof C:e&&"object"===(void 0===e?"undefined":k(e))&&"number"==typeof e.nodeType&&"string"==typeof e.nodeName},je=function(e,t,n){J[e]&&J[e].forEach(function(e){e.call(u,t,n,_e)})},Pe=function(e){var t=void 0;if(je("beforeSanitizeElements",e,null),ze(e))return Re(e),!0;var n=e.nodeName.toLowerCase();if(je("uponSanitizeElement",e,{tagName:n,allowedTags:ie}),!ie[n]||se[n]){if(Le&&!ke[n]&&"function"==typeof e.insertAdjacentHTML)try{var r=e.innerHTML;e.insertAdjacentHTML("AfterEnd",B?B.createHTML(r):r)}catch(e){}return Re(e),!0}return"noscript"===n&&e.innerHTML.match(/<\/noscript/i)?(Re(e),!0):"noembed"===n&&e.innerHTML.match(/<\/noembed/i)?(Re(e),!0):(!me||e.firstElementChild||e.content&&e.content.firstElementChild||!/</g.test(e.textContent)||(u.removed.push({element:e.cloneNode()}),e.innerHTML?e.innerHTML=e.innerHTML.replace(/</g,"&lt;"):e.innerHTML=e.textContent.replace(/</g,"&lt;")),he&&3===e.nodeType&&(t=(t=(t=e.textContent).replace(Q," ")).replace(Z," "),e.textContent!==t&&(u.removed.push({element:e.cloneNode()}),e.textContent=t)),je("afterSanitizeElements",e,null),!1)},Ue=function(e,t,n){if(xe&&("id"===t||"name"===t)&&(n in v||n in Ne))return!1;if(fe&&ee.test(t));else if(de&&te.test(t));else{if(!le[t]||ue[t])return!1;if(Ee[t]);else if(oe.test(n.replace(re,"")));else if("src"!==t&&"xlink:href"!==t&&"href"!==t||"script"===e||0!==n.indexOf("data:")||!we[e]){if(pe&&!ne.test(n.replace(re,"")));else if(n)return!1}else;}return!0},We=function(e){var t=void 0,n=void 0,r=void 0,o=void 0,i=void 0;je("beforeSanitizeAttributes",e,null);var a=e.attributes;if(a){var l={attrName:"",attrValue:"",keepAttr:!0,allowedAttributes:le};for(i=a.length;i--;){var c=t=a[i],s=c.name,d=c.namespaceURI;if(n=t.value.trim(),r=s.toLowerCase(),l.attrName=r,l.attrValue=n,l.keepAttr=!0,je("uponSanitizeAttribute",e,l),n=l.attrValue,"name"===r&&"IMG"===e.nodeName&&a.id)o=a.id,a=w(E,a,[]),Ce("id",e),Ce(s,e),a.indexOf(o)>i&&e.setAttribute("id",o.value);else{if("INPUT"===e.nodeName&&"type"===r&&"file"===n&&l.keepAttr&&(le[r]||!ue[r]))continue;"id"===s&&e.setAttribute(s,""),Ce(s,e)}if(l.keepAttr){he&&(n=(n=n.replace(Q," ")).replace(Z," "));var f=e.nodeName.toLowerCase();if(Ue(f,r,n))try{d?e.setAttributeNS(d,s,n):e.setAttribute(s,n),u.removed.pop()}catch(e){}}}je("afterSanitizeAttributes",e,null)}},Be=function e(t){var n=void 0,r=Fe(t);for(je("beforeSanitizeShadowDOM",t,null);n=r.nextNode();)je("uponSanitizeShadowNode",n,null),Pe(n)||(n.content instanceof D&&e(n.content),We(n));je("afterSanitizeShadowDOM",t,null)};return u.sanitize=function(e,t){var n=void 0,r=void 0,i=void 0,a=void 0,l=void 0;if(e||(e="\x3c!--\x3e"),"string"!=typeof e&&!Ie(e)){if("function"!=typeof e.toString)throw new TypeError("toString is not a function");if("string"!=typeof(e=e.toString()))throw new TypeError("dirty is not a string, aborting")}if(!u.isSupported){if("object"===k(o.toStaticHTML)||"function"==typeof o.toStaticHTML){if("string"==typeof e)return o.toStaticHTML(e);if(Ie(e))return o.toStaticHTML(e.outerHTML)}return e}if(ge||De(t),u.removed=[],Se);else if(e instanceof C)1===(r=(n=He("\x3c!--\x3e")).ownerDocument.importNode(e,!0)).nodeType&&"BODY"===r.nodeName?n=r:"HTML"===r.nodeName?n=r:n.appendChild(r);else{if(!be&&!he&&!ye&&-1===e.indexOf("<"))return B?B.createHTML(e):e;if(!(n=He(e)))return be?null:G}n&&ve&&Re(n.firstChild);for(var c=Fe(Se?e:n);i=c.nextNode();)3===i.nodeType&&i===a||Pe(i)||(i.content instanceof D&&Be(i.content),We(i),a=i);if(a=null,Se)return e;if(be){if(Te)for(l=X.call(n.ownerDocument);n.firstChild;)l.appendChild(n.firstChild);else l=n;return Ae&&(l=$.call(h,l,!0)),l}var s=ye?n.outerHTML:n.innerHTML;return he&&(s=(s=s.replace(Q," ")).replace(Z," ")),B?B.createHTML(s):s},u.setConfig=function(e){De(e),ge=!0},u.clearConfig=function(){_e=null,ge=!1},u.isValidAttribute=function(e,t,n){_e||De({});var r=e.toLowerCase(),o=t.toLowerCase();return Ue(r,o,n)},u.addHook=function(e,t){"function"==typeof t&&(J[e]=J[e]||[],J[e].push(t))},u.removeHook=function(e){J[e]&&J[e].pop()},u.removeHooks=function(e){J[e]&&(J[e]=[])},u.removeAllHooks=function(){J={}},u}var o=Object.freeze||function(e){return e},i=o(["a","abbr","acronym","address","area","article","aside","audio","b","bdi","bdo","big","blink","blockquote","body","br","button","canvas","caption","center","cite","code","col","colgroup","content","data","datalist","dd","decorator","del","details","dfn","dir","div","dl","dt","element","em","fieldset","figcaption","figure","font","footer","form","h1","h2","h3","h4","h5","h6","head","header","hgroup","hr","html","i","img","input","ins","kbd","label","legend","li","main","map","mark","marquee","menu","menuitem","meter","nav","nobr","ol","optgroup","option","output","p","pre","progress","q","rp","rt","ruby","s","samp","section","select","shadow","small","source","spacer","span","strike","strong","style","sub","summary","sup","table","tbody","td","template","textarea","tfoot","th","thead","time","tr","track","tt","u","ul","var","video","wbr"]),a=o(["svg","a","altglyph","altglyphdef","altglyphitem","animatecolor","animatemotion","animatetransform","audio","canvas","circle","clippath","defs","desc","ellipse","filter","font","g","glyph","glyphref","hkern","image","line","lineargradient","marker","mask","metadata","mpath","path","pattern","polygon","polyline","radialgradient","rect","stop","style","switch","symbol","text","textpath","title","tref","tspan","video","view","vkern"]),l=o(["feBlend","feColorMatrix","feComponentTransfer","feComposite","feConvolveMatrix","feDiffuseLighting","feDisplacementMap","feDistantLight","feFlood","feFuncA","feFuncB","feFuncG","feFuncR","feGaussianBlur","feMerge","feMergeNode","feMorphology","feOffset","fePointLight","feSpecularLighting","feSpotLight","feTile","feTurbulence"]),c=o(["math","menclose","merror","mfenced","mfrac","mglyph","mi","mlabeledtr","mmultiscripts","mn","mo","mover","mpadded","mphantom","mroot","mrow","ms","mspace","msqrt","mstyle","msub","msup","msubsup","mtable","mtd","mtext","mtr","munder","munderover"]),s=o(["#text"]),u=Object.freeze||function(e){return e},d=u(["accept","action","align","alt","autocomplete","background","bgcolor","border","cellpadding","cellspacing","checked","cite","class","clear","color","cols","colspan","controls","coords","crossorigin","datetime","default","dir","disabled","download","enctype","face","for","headers","height","hidden","high","href","hreflang","id","integrity","ismap","label","lang","list","loop","low","max","maxlength","media","method","min","minlength","multiple","name","noshade","novalidate","nowrap","open","optimum","pattern","placeholder","poster","preload","pubdate","radiogroup","readonly","rel","required","rev","reversed","role","rows","rowspan","spellcheck","scope","selected","shape","size","sizes","span","srclang","start","src","srcset","step","style","summary","tabindex","title","type","usemap","valign","value","width","xmlns"]),f=u(["accent-height","accumulate","additive","alignment-baseline","ascent","attributename","attributetype","azimuth","basefrequency","baseline-shift","begin","bias","by","class","clip","clip-path","clip-rule","color","color-interpolation","color-interpolation-filters","color-profile","color-rendering","cx","cy","d","dx","dy","diffuseconstant","direction","display","divisor","dur","edgemode","elevation","end","fill","fill-opacity","fill-rule","filter","filterunits","flood-color","flood-opacity","font-family","font-size","font-size-adjust","font-stretch","font-style","font-variant","font-weight","fx","fy","g1","g2","glyph-name","glyphref","gradientunits","gradienttransform","height","href","id","image-rendering","in","in2","k","k1","k2","k3","k4","kerning","keypoints","keysplines","keytimes","lang","lengthadjust","letter-spacing","kernelmatrix","kernelunitlength","lighting-color","local","marker-end","marker-mid","marker-start","markerheight","markerunits","markerwidth","maskcontentunits","maskunits","max","mask","media","method","mode","min","name","numoctaves","offset","operator","opacity","order","orient","orientation","origin","overflow","paint-order","path","pathlength","patterncontentunits","patterntransform","patternunits","points","preservealpha","preserveaspectratio","primitiveunits","r","rx","ry","radius","refx","refy","repeatcount","repeatdur","restart","result","rotate","scale","seed","shape-rendering","specularconstant","specularexponent","spreadmethod","stddeviation","stitchtiles","stop-color","stop-opacity","stroke-dasharray","stroke-dashoffset","stroke-linecap","stroke-linejoin","stroke-miterlimit","stroke-opacity","stroke","stroke-width","style","surfacescale","tabindex","targetx","targety","transform","text-anchor","text-decoration","text-rendering","textlength","type","u1","u2","unicode","values","viewbox","visibility","version","vert-adv-y","vert-origin-x","vert-origin-y","width","word-spacing","wrap","writing-mode","xchannelselector","ychannelselector","x","x1","x2","xmlns","y","y1","y2","z","zoomandpan"]),p=u(["accent","accentunder","align","bevelled","close","columnsalign","columnlines","columnspan","denomalign","depth","dir","display","displaystyle","fence","frame","height","href","id","largeop","length","linethickness","lspace","lquote","mathbackground","mathcolor","mathsize","mathvariant","maxsize","minsize","movablelimits","notation","numalign","open","rowalign","rowlines","rowspacing","rowspan","rspace","rquote","scriptlevel","scriptminsize","scriptsizemultiplier","selection","separator","separators","stretchy","subscriptshift","supscriptshift","symmetric","voffset","width","xmlns"]),m=u(["xlink:href","xml:id","xlink:title","xml:space","xmlns:xlink"]),h=Object.hasOwnProperty,y=Object.setPrototypeOf,g=("undefined"!=typeof Reflect&&Reflect).apply;g||(g=function(e,t,n){return e.apply(t,n)});var v=Object.seal||function(e){return e},b=v(/\{\{[\s\S]*|[\s\S]*\}\}/gm),T=v(/<%[\s\S]*|[\s\S]*%>/gm),A=v(/^data-[\-\w.\u00B7-\uFFFF]/),x=v(/^aria-[\-\w]+$/),L=v(/^(?:(?:(?:f|ht)tps?|mailto|tel|callto|cid|xmpp):|[^a-z]|[a-z+.\-]+(?:[^a-z+.\-:]|$))/i),S=v(/^(?:\w+script|data):/i),M=v(/[\u0000-\u0020\u00A0\u1680\u180E\u2000-\u2029\u205f\u3000]/g),k="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},w=("undefined"!=typeof Reflect&&Reflect).apply,E=Array.prototype.slice,O=Object.freeze,_=function(){return"undefined"==typeof window?null:window};w||(w=function(e,t,n){return e.apply(t,n)});var N=function(e,t){if("object"!==(void 0===e?"undefined":k(e))||"function"!=typeof e.createPolicy)return null;var n=null;t.currentScript&&t.currentScript.hasAttribute("data-tt-policy-suffix")&&(n=t.currentScript.getAttribute("data-tt-policy-suffix"));var r="dompurify"+(n?"#"+n:"");try{return e.createPolicy(r,{createHTML:function(e){return e}})}catch(e){return console.warn("TrustedTypes policy "+r+" could not be created."),null}};return r()});
