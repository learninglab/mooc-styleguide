# Templates pour avoir les bons icônes dans FUN
## Comment ça marche
On définit sur chaque unité un attribut "data-unit-icon" (de préférence dans le titre) avec le nom de l'icone souhaité, exemple :
- data-unit-icon="fa-comments-o" pour un onglet forum
- data-unit-icon="ill-icon-video" pour un onglet video (les icons feather icons doivent avoir le prefixe "ill-icon")

Quand on affiche le titre, on choisit aussi son icone. Plein d'exemples plus bas.

## bibliothèques d'icones
On a le choix entre 2 bibliothèques :

- bibliotheque feathericons https://feathericons.com/

```<i class="fa fa-edit"></i>```

- bibliotheque FUN : https://fontawesome.com/v4.7/icons/

```<i class="ill-icon ill-icon-file-text"></i>```

## Pour tous les titres, il faut déclarer la css et le js :
```
<link rel="stylesheet" type="text/css" href="/static/ill-styleguide.css">
<script src="/static/ill-styleguide.js"></script>
```
## Fiches concepts
```
<section class="styleguide-section" data-unit-icon="ill-icon-file-text">
  <div class="styleguide-content">
    <h3 class="title-banner">
      	<i class="ill-icon ill-icon-file-text"></i>
        <span>Titre</span>
    </h3>
  </div>
</section>
<div class="external-resource" data-url="https://learninglab.gitlabpages.inria.fr/mooc-impacts-num/mooc-impacts-num-ressources/Partie2/FichesConcept/FC2.1.1-LeNumeriqueQuelsChiffres-MoocImpactNum.html" data-hide=".headerlink, .md-sidebar, .md-skip, .md-toggle, .md-overlay, nav, header, h1,footer,.md-content__button"></div>
```
## Quiz
```
<section class="styleguide-section" data-unit-icon="fa-list-ul">
  <div class="styleguide-content">
    <h3 class="title-banner">
          <i class="fa fa-list-ul"></i>
          <span>Quiz i</span>
    </h3>
  </div>
</section>
```

## Activités
```
<section class="styleguide-section" data-unit-icon="fa-edit">
  <div class="styleguide-content">
    <h3 class="title-banner">
          <i class="fa fa-edit"></i>
          <span>Titre</span>
    </h3>
  </div>
</section>
```
## Introduction/Conclusion
```
<section class="styleguide-section" data-unit-icon="fa-bookmark">
  <div class="styleguide-content">
    <h3 class="title-banner">
          <i class="fa fa-bookmark"></i>
          <span>Introduction ou Titre</span>
    </h3>
  </div>
</section>
```
## Video
```
<section class="styleguide-section" data-unit-icon="ill-icon-video">
  <div class="styleguide-content">
    <h3 class="title-banner">
      <i class="ill-icon ill-icon-video"></i>
        <span>Titre</span>
    </h3>
  </div>
</section>
```
## Forum
```
<section class="styleguide-section" data-unit-icon="fa-comments-o">
  <div class="styleguide-content">
    <h3 class="title-banner">
          <i class="fa fa-comments-o"></i>
        <span>Forum</span>
    </h3>
  </div>
</section>
```
## Pour en savoir plus ou Ressources complémentaire
```
<section class="styleguide-section" data-unit-icon="ill-icon-link">
  <div class="styleguide-content">
    <h3 class="title-banner">
      <i class="ill-icon ill-icon-link"></i>
        <span>Ressources complémentaires</span>
    </h3>
  </div>
</section>
```
## Guides
```
<section class="styleguide-section" data-unit-icon="ill-icon-navigation">
  <div class="styleguide-content">
    <h3 class="title-banner">
      <i class="ill-icon ill-icon-navigation"></i>
        <span>Guide</span>
    </h3>
  </div>
</section>
```

## Insertion d'une page dans FUN
Avec le titre+lien vers la fiche dans le portail
``` html
<link rel="stylesheet" type="text/css" href="/static/ill-styleguide.css">
<script src="/static/ill-styleguide.js"></script>

<div class="external-resource" data-unit-icon="fa-search-plus" data-title-prefix="Fiche concept : " data-url="https://learninglab.gitlabpages.inria.fr/mooc-impacts-num/mooc-impacts-num-ressources/Partie1/FichesConcept/FC1.2.1-CEstQuoiLeNumerique-MoocImpactNum.html" data-hide=".headerlink, .md-sidebar, .md-skip, .md-toggle, .md-overlay, nav, header, h1,footer"></div>
```